# Recipos

An automated dinner planner.

Add your own recipes and ingredients which the tool will choose from and use to plan out your week and generate a shopping list.

## Features

 * Add your own recipes and ingredients
 * Keeps a history of choosen recipes to prevent repetitiveness

### Prerequisites

```
python3
```

### Running and Setup 

To execute the script one must first create a recipe directory and populate it with your own recipes (some examples are provided (Note that recipe names must match the filename)).

```
mkdir recipos; cd reciops;
wget https://gitlab.com/aradl/SOMETHING;
mkdir recipes;
```
To Run:

```
./recipos.py
```

Output Example:

```
Mon
    BLAH

Tue
    BLAH

...etc 
```

## Built With

 * [Python3](http://www.python.org) - Language
 * [any plugins?]() - ??

## Authors

 * **Andrew** - [aradl](https://gitlab.com/aradl)
