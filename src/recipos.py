#!/usr/bin/env python3
"""
    Recipos

    created with python3 syntax
    #TODO find a way to reconize this and exit if not python3
"""

import os;
import sys;

import json;
import random;

import datetime;
import time;

TEST_MODE = False 
RECIPE_DIR = "recipes"
DAYS_FOR_MEALS = 5
MEALS_IN_A_DAY = 1
exit_string = "\n"

def exit_program( str ):
    print(str)
    sys.exit() 
    
readable_current_time = time.strftime("%m/%d/%Y")
current_time = datetime.datetime.now().isoformat()
print("\tWelcome to Recipos")
print('\t ', readable_current_time, '\n')

# loop through the recipes folder and store each one in a list
# exit if the directory doesn't exist
if not os.path.isdir(RECIPE_DIR):
    exit_string += "ERROR: '" + RECIPE_DIR + "' directory does not exist" + '\n'
    exit_program(exit_string)

recipe_list = []
for filename in os.listdir(RECIPE_DIR):
    if filename.endswith(".json"):
        #print(os.path.join(directory, filename))
        #TODO add error handling with file opening
        with open(os.path.join(RECIPE_DIR, filename)) as data_file:
            # data is of type dict
            data = json.load(data_file)
            # throw out recently used recipies
            if 'date_last_used' in data:
                #TODO EX then = '2017-02-24T12:12:29.264290'
                recipe_time = data['date_last_used']
                # convert time into a string using the std ISO format as returned by datetime.datetime.now().isoformat()
                then = time.strptime(recipe_time, '%Y-%m-%dT%H:%M:%S.%f')
                now = time.strptime(current_time, '%Y-%m-%dT%H:%M:%S.%f')
                # convert to time objects to allow for simple math
                then = time.mktime(then)
                now = time.mktime(now)
                # find the difference in the amount of days
                diff = now - then 
                days = int(diff) / 86400

                if days > DAYS_FOR_MEALS:
                    recipe_list.append(data)

            else:
                recipe_list.append(data)
        continue
    else:
        continue

# choose 5 random unique numbers between 0 and the amount of recipes; if you can
#for i in recipe_list:      #DEBUG
#    print(json.dumps(i))   #DEBUG

recipe_amount = len(recipe_list)    #TODO think abount how you are handling errors, have you gotten all of them?
recipes_chosen = []
if recipe_amount > 0:
    if recipe_amount >= DAYS_FOR_MEALS:
        # chose a DAYS_FOR_MEALS amount of unique random values 
        try:
            recipes_chosen = random.sample(range(0, recipe_amount), DAYS_FOR_MEALS)
        except ValueError:
            exit_string += "ERROR: you shouldn't have gotten there" + '\n'
    else:
        exit_string += "INFO: You need more recipes!! (i.e. you have less than " + str(DAYS_FOR_MEALS) + " recipes)"  + '\n'
        for i in range (0, DAYS_FOR_MEALS):
            recipes_chosen.append(random.randint(0, (recipe_amount - 1)))

else:
    exit_string += "ERROR: You need to add some recipes to the folder: " + RECIPE_DIR + '\n'
    exit_program(exit_string)

#TODO implement a calender system and format a pretty cal output
#TODO temp list with weekday names, make more dynamic
weekdays = ["Mon", "Tues", "Wed", "Thurs", "Fri"]
grocery_list = []
index = 0

# display choosen recipies
for i in recipes_chosen:
    # output meals chosen with day/date
    print(weekdays[index])
    index += 1

    recipe = recipe_list[i]
    recipe_name = recipe['name'] 
    print('\t', recipe_name)

# ask user to commit to the plan and update the dates
to_commit = input("\nAre you happy with the plan? \n (Enter n/no to exit, anything else will continue): ")
if to_commit == "n" or to_commit == "no":
    exit_program(exit_string)


# update recipes last used date and generate grocery list
for i in recipes_chosen:

    recipe = recipe_list[i]
    recipe_name = recipe['name'] 
    # add a date to the recipe dict
    recipe.update(date_last_used=current_time)

    # save updated recipe into new file
    if not TEST_MODE:
        with open(os.path.join(RECIPE_DIR, recipe_name + ".json"), 'w') as outfile:
            json.dump(recipe, outfile)
            exit_string += "INFO: Updated File: " + recipe_name + ".json" + '\n'

    # sort ingredients into a shopping list
    #TODO create a composite list off all ingredients (for now just show quantity of ones with the same name)
    ingredients_list = recipe_list[i]['ingredients']
    for j in ingredients_list:
        grocery_item = j
        if grocery_list.count(grocery_item) == 0:
            grocery_list.append(grocery_item)


# print out grocery list           
print('\n', "THE LIST: ", '\n')
for i in grocery_list:
   print('\t', i)

exit_program(exit_string)
